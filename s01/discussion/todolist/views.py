from django.shortcuts import render

# Create your views here.

#the from keyword allows importing of necessary claases/modules, methods and other files needed in our application from the django.http package while the import keyword defines that we are importing from the package.
from django.http import HttpResponse

def index(request):
	return HttpResponse("Hello from the views.py file!")